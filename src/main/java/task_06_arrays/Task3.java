package task_03_Arrays_Collections1;

public class Task3_Arrays_Collections1 {
    public static void main(String[] args) {
        TestProgramm task=new TestProgramm();
        task.makePriorityQueue();
        int arrayOne[]= {1,3,4,5,6,6,8,11};
        int arrayTwo[]= {19,5,6,6,8,8,11,11,11};
        task.arraySameElements(arrayOne, arrayTwo);
        System.out.println();
        task.arrayDeleteCaples(arrayTwo);
        System.out.println();
        task.arrayDeleteSequanseOfElementsAndLeaveOne(arrayTwo);
        System.out.println();
        task.game();

    }
}
